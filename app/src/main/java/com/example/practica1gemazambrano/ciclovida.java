package com.example.practica1gemazambrano;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class ciclovida extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ciclovida);
        Log.e("ciclo de vida","onCreate");
        MostrarMensaje("onCreate");
    }

    private void MostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("ciclo de vida","onStart");
        MostrarMensaje("onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("ciclo de vida","onPause");
        MostrarMensaje("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("ciclo de vida","onResume");
        MostrarMensaje("onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("ciclo de vida","onDestroy");
        MostrarMensaje("onDestroy");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("ciclo de vida","onStop");
        MostrarMensaje("onStop");
    }

}
